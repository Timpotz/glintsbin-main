# Week 1.3  Lessons Summary

## Git Introduction

# Summary of today's lesson:
```sh
  - Explanation of what Git is
  - How to use upload file to gitlab
  - Commands used in Git from terminal
 
```

Commands used today:
```sh
    - git init
    - git pull
    - git push
    - git branch
    - git add
```