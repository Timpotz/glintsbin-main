# Week 1.1  Lessons Summary

# Introduction

# Summary of today's lesson:
```sh
  - Explanation of what a backend engineeer is and what their jobs are.
  - Navigatiing through directories with the Linux terminal
  - Creating files and directories in Linux 
  - How to use text editor (VS)
  - Using node on terminal
```

Commands used today:
```sh
    - pwd to show the current path
    - ls to show list of directory content
    - rm to remove file/directory
    - mkdir to create directory
    - touch to create file
    - cat to show content of file
```
Node JS:
```sh
    console.log("Hello World!")
```