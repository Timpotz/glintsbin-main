# Week 1.4  Lessons Summary

## Git

# Summary of today's lesson:
```sh
  - Collaboration work using git lab
  - Simple javascript
```

Javascript used today:
```sh
    | Shape | Link |
    | ------ | ------ |
    | Circle | [https://gitlab.com/Nattooo/session-4-team-a/-/blob/master/circle.js] |
    | Cube | [https://gitlab.com/Nattooo/session-4-team-a/-/blob/master/cube.js] |
    | Square | [https://gitlab.com/Nattooo/session-4-team-a/-/blob/master/square.js] | 
    | Triangle |[https://gitlab.com/Nattooo/session-4-team-a/-/blob/triangle/triangle.js] |       
```
