const data = [];
const randomNumber = Math.floor(Math.random() * 100);

function createArray() {
  for (let i = 0; i < randomNumber; i++) {
    data.push(createArrayElement())
  }

  // Recursive
  if (data.length == 0) {
    createArray();
  }
}

function createArrayElement() {
  let random = Math.floor(Math.random() * 1000);

  return [null, random][Math.floor(Math.random() * 2)]
}

createArray()

/*
 * Code Here!
 * */
console.log(data);
// 1st Option
function clean(data) {
    // Store Variabel in temp
    let temp = [];
    //Traverese Thorugh all index in data array
    for(let i of data) {
        // Copy Each Number without Null Into "temp" variable
        i && temp.push(i);
        // Update the "Variable" with assign temp array into data variable
        data = temp;
    }
    return data
}
// 2nd Option in Easy way to describe using While loop
function clean2(data) {
  let i = 0;
  let temp = [];
  while(i < data.length) {
    if(data[i]) { //COndisi harus true
      temp.push(data[i])
    }
    i++;
  }
  data = temp;
  return data;
}
// 3rd Option With For Loops
function clean3(data){
  let temp = [];
  for(let i = 0; i < data.length; i++) {
    if(data[i]) {
      temp.push(data[i])
    }
  }
  console.log(temp)
}
clean3(data)
/*
 * DON'T CHANGE
 * */

if (process.argv.slice(2)[0] == "test") {
  try {
    clean(data).forEach(i => {
      if (i == null) {
        throw new Error("Array still contains null")
      }
    })
  }

  catch(err) {
    console.error(err.message);
  }
}
