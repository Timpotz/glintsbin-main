const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
console.clear();
console.log("Which One Do you Want to calculate");
console.log(`
1. Circle Area,
2. Round Of Circle`)  

function areaOfCircle(r){
    r = Number(r);
    if(typeof(r) === 'number') {
        const hasil = 3.14 * (r**2);
        return hasil;
    } else {
        throw new Error('Must be number')
    }
};
function roundOfCircle(r) {
    r = Number(r)
    if(typeof(r) === 'number') {
    const round = Math.floor(2 * 3.14 * r);
    return round;
    } else {
        throw new Error('Must be number')
    }
};

const handleAnswer1 = () => {
    console.clear();
    console.log('Calculate Circle Area');
    rl.question('Radius: ', r => {
            let userOutput = areaOfCircle(r);
            if(isNaN(userOutput)) {
                console.log(`Is ${r} a number?`)
                rl.close()
            } else {
                console.log("Here's The Result: "+ userOutput);
                rl.close();
            }
        }
    )
}
const handleAnswer2 = () => {
    console.clear();
    console.log('Calculate Round Of Circle')
    rl.question('Radius: ', r => {
            let userOutput = roundOfCircle(r);
            if(isNaN(userOutput)) {
                console.log(`Opo Bener ${r} sama dengan angka Blok ?`)
                rl.close();
            } else {
                console.log("Here's The Result: " + userOutput);
                rl.close();
            }
        }
    )
}
rl.question('Answer: ', answer => {
    if (answer == 1) {
      handleAnswer1();
    }  
    else if (answer == 2) {
      handleAnswer2();
    }
    else {
      console.log("Sorry, there's no option for that!")
      rl.close()
    }
  });

