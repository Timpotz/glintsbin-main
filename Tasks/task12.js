const fs = require("fs");

class Record {
  constructor(props) {
    if (this.constructor == Record)
      throw new Error("Can't instantiate from Record");

    this._validate(props);
    this._set(props);
  }

  _validate(props) {
    if (typeof props !== "object" || Array.isArray(props))
      throw new Error("Props must be an object");

    this.constructor.properties.forEach((i) => {
      if (!Object.keys(props).includes(i))
        throw new Error(`${this.constructor.name}: ${i} is required`);
    });
  }

  _set(props) {
    this.constructor.properties.forEach((i) => {
      this[i] = props[i];
    });
  }

  get all() {
    try {
      return require(`${__dirname}/${this.constructor.name}.json`);
    } catch {
      return [];
    }
  }

  find(id) {}

  update(id) {}

  delete(id) {}

  save() {
    let record = [...this.all];
    record.push({ id: this.all.length + 1, ...this });

    fs.writeFileSync(
      `${__dirname}/${this.constructor.name}.json`,
      JSON.stringify(record, null, 2)
    );
  }
}

class User extends Record {
  static properties = ["email", "password"];

  constructor(props) {
    super(props);
  }
}

let Fikri = new User({
  email: "test01@mail.com",
  password: "123456",
});

class Book extends Record {
  static properties = ["title", "author", "price", "publisher"];

  constructor(props) {
    super(props);
  }
}

let Book1 = new Book({
  title: "xxx",
  author: "urmomma",
  price: "5 bucks",
  publisher: "lol publishing",
});

let Book2 = new Book({
  title: "noob",
  author: "motherfck",
  price: "10 bucks",
  publisher: "noshit publishing",
});
Book1.save();
Book2.save();

class Product extends Record {
  static properties = ["name", "price", "stock"];

  constructor(props) {
    super(props);
  }
}

let ProductA = new Product({
  name: "xxx",
  price: "5 bucks",
  stock: "5",
});

let ProductB = new Product({
  name: "abc",
  price: "10 bucks",
  stock: "3",
});

ProductA.save();
ProductB.save();
/*

  Make two class who inherit Abstract Class called Record 

  Book,
    title
    author
    price
    publisher

  Product,
    name,
    price,
    stock
*/
