const express = require("express");
const Joi = require("joi");
const app = express();
const api = [
  { id: 1, name: "shit" },
  { id: 2, name: "fuck" },
  { id: 3, name: "cunt" },
  { id: 4, name: "ass" },
];

//middleware
app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello World");
});

app.get("/api", (req, res) => {
  res.send(api);
});

app.get("/api/:id", (req, res) => {
  const apis = api.find((c) => c.id === parseInt(req.params.id));
  if (!apis) res.status(404).send("The api was not found");
  res.send(apis);
});

app.post("/api/apis", (req, res) => {
  const schema = {
    name: Joi.string().min(3).required(),
  };
  //JOI VALIDATION
  const result = Joi.validate(req.body, schema);
  console.log(result);

  if (result.error) {
    //400 bad request
    res.status(400).send(result.error.details[0].message);
    return;
  }
  const apis = {
    id: api.length + 1,
    name: req.body.name,
  };
  api.push(apis);
  res.send(apis);
});
// PORT
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening to port ${port}`));
