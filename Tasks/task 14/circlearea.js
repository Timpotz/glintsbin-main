function areaOfCircle(r) {
  r = Number(r);
  if (typeof r === "number") {
    const hasil = 3.14 * r ** 2;
    return hasil;
  } else {
    throw new Error("Angka Cok");
  }
}
function roundOfCircle(r) {
  r = Number(r);
  if (typeof r === "number") {
    const round = Math.floor(2 * 3.14 * r);
    return round;
  } else {
    throw new Error("Sudah Dibilang Harus angka Kok sek ngeyel");
  }
}

module.exports = {
  areaOfCircle,
  roundOfCircle,
};

console.log(areaOfCircle(5));
console.log(roundOfCircle(5));
