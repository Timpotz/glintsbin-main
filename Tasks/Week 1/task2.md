Week 1.2  Lessons Summary

The Internet

Summary of today's lesson:
  - Explanation of what the internet is
  - How to ping servers
  - What is an IP address
  - DNS and how it searches for IP address according to the website domain
 
Commands used today:
    - ping